package skills;

import java.util.List;

public class ListToDoTaskSkill extends AbstractSkill {

    @Override
    public void run() {
        List<String> tasks = accountInfo.getTasks();
        System.out.println("ALEXA: tasks are: ");
        tasks.stream().forEach((task) -> {
            System.out.println(task);
        });
    }
}
