package skills;

import skills.AbstractSkill;

public class DontKnowSkill extends AbstractSkill {

    public DontKnowSkill() {
    }

    @Override
    public void run() {
        System.out.println("ALEXA: I don't know such a command");
    }
}
