package skills;

import alexa.AlexaAccountInfo;

public abstract class AbstractSkill implements Runnable {

    protected AlexaAccountInfo accountInfo;

    public void invokeUsing(AlexaAccountInfo info){
        accountInfo = info;
    }
}
