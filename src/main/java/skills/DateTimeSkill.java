package skills;

import skills.AbstractSkill;

import java.time.LocalDateTime;

public class DateTimeSkill extends AbstractSkill {

    public DateTimeSkill() {
    }

    @Override
    public void run() {
        System.out.println("ALEXA: the time is: " + LocalDateTime.now());
    }
}
