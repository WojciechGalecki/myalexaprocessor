package skills;

public class AddToDoTaskSkill extends AbstractSkill {

    private String task;

    public AddToDoTaskSkill(String task) {
        this.task = task;
    }

    @Override
    public void run() {
        if(!task.trim().isEmpty()){
            accountInfo.getTasks().add(task);
            System.out.println("ALEXA: added \" " + task + "\" to todo tasks.");
        } else {
            System.out.println("ALEXA: Say the task You want to add");
        }
    }
}
