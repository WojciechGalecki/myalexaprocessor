package alexa;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        AlexaDevice device = new AlexaDevice();
        AlexaServer server = new AlexaServer();

        while(sc.hasNextLine()){
            String line = sc.nextLine();
            if(line.equalsIgnoreCase("register")) {
                if(device.countObservers() == 0){
                    device.addObserver(server);
                }
            } else if(line.equalsIgnoreCase("unregister")){
                device.deleteObserver(server);
            } else{
                device.sendRequest(line);
            }
        }
    }
}
