package alexa;

import skills.AbstractSkill;

import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AlexaDevice extends Observable {

    private Executor skillsThreads = Executors.newFixedThreadPool(1);
    private AlexaAccountInfo info = AlexaAccountInfo.getInstance();

    private static int alexaIds = 0;
    private int id = alexaIds;

    public AlexaDevice() {
    }

    public void sendRequest(String command){
        Request request = new Request(command,this);
        setChanged();
        notifyObservers(request);
    }

    public void invoke(AbstractSkill callback){
        callback.invokeUsing(info);
        skillsThreads.execute(callback);
    }
}
