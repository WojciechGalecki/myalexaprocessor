package alexa;

import skills.*;

import java.util.Observable;
import java.util.Observer;

public class AlexaServer implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Request){
            Request casted = (Request)arg;
            AbstractSkill callback = parseRequest(casted.getRequest());
            casted.getDeviceToCall().invoke(callback);
        }

    }

    public AbstractSkill parseRequest(String request){
        String req = request.toLowerCase();
        if(req.startsWith("alexa")){
            req = req.substring(6);
            if(req.startsWith("add todo task")){
                req = req.substring(14);
                String task = req;
                return new AddToDoTaskSkill(task);
            }
            if(req.startsWith("remove todo task")){
                req = req.substring(17);
                String task = req;
                return new RemoveToDoTaskSkill(task);
            }
            if(req.startsWith("list tasks")){
                return new ListToDoTaskSkill();
            }
            if(req.startsWith("what's the time")||
                    req.startsWith("give me the time")||
                    req.startsWith("the time")||
                    req.startsWith("what time is it")){
                return new DateTimeSkill();
            }
        }
        return new DontKnowSkill();
    }
}

