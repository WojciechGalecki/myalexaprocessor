package alexa;

import java.util.ArrayList;
import java.util.List;

public class AlexaAccountInfo {

    private List<String> tasks;
    private static AlexaAccountInfo instance;

    private AlexaAccountInfo(){
        tasks = new ArrayList<>();
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    public static synchronized AlexaAccountInfo getInstance(){
        if(instance == null){
            synchronized (AlexaAccountInfo.class){
                if(instance == null){
                    instance = new AlexaAccountInfo();
                }
            }
        }
        return instance;
    }
}
